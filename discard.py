#!/usr/bin/env python3

import os
import logging
from selenium import webdriver
from pyvirtualdisplay import Display



def main():
    visible = 0

    if 'DEBUG_LEVEL' in os.environ and os.environ['DEBUG_LEVEL'] == 'debug':
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.ERROR)

    if 'PASSWD' in os.environ:
        passwd = os.environ['PASSWD']
    elif visible == 1:
        passwd = "xxx"
    else:
        logging.error("No password available in PASSWD environment variable.")
        sys.exit(3)
     
    display = Display(visible=visible)
    display.start()
    browser = webdriver.Firefox()


    browser.get('https://mailman.muni.cz/mailman/admindb/h2020-westlife')

# Login page
    pwdform = browser.find_element_by_name("adminpw")
    pwdform.send_keys(passwd)
    tlacitko = browser.find_element_by_name("admlogin")
    tlacitko.click()

# Requests page
    hasElement = True
    while hasElement:
        try:
            tlacitko_discard = browser.find_element_by_xpath("//input[@value='3']")
            tlacitko_discard.click()
            submit = browser.find_element_by_name("submit")
            submit.click()
        except:
            hasElement = False

    browser.close()


if __name__ == '__main__':
    main()
