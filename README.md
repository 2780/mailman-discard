# Automatic mailing list requests removal 

Following environment variables are used:

*  **PASSWD** - set password
*  **DEBUG_LEVEL** - set to "debug" to get more debug information


Start with
```export PASSWD=xxx ```

```python3 discard.py```

or build a container and run it

```docker build . -t mailman_diskard```

```docker run -e PASSWD=xxx mailman_diskard```

