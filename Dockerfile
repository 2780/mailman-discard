FROM ubuntu:18.04
LABEL maintainer=pesa@ics.muni.cz

ARG geckodriver_tar_file=geckodriver-v0.26.0-linux64.tar.gz
RUN apt-get update && apt-get install -y python3-pip xvfb firefox x11-utils

ADD https://github.com/mozilla/geckodriver/releases/download/v0.26.0/${geckodriver_tar_file} /tmp
RUN tar -zxvf /tmp/${geckodriver_tar_file} --directory /usr/bin/ && \
    rm /tmp/${geckodriver_tar_file}

RUN mkdir /mailman-discard
COPY . /mailman-discard

WORKDIR /mailman-discard
RUN chmod +x discard.py
RUN pip3 install -r /mailman-discard/requirements.txt

CMD ["./discard.py"]
